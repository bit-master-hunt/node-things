var db = require('./db');
var mongo = require('mongodb');
var Thing = require('./thingModel');
var COLLECTION = 'things';

function transformThing( thing ) {
   if( thing ) {
      thing.id = thing._id;
      delete thing._id;
   }
   
   return thing;
}

function create( ownerId, name, value, cb ) {
   var result = new Thing( ownerId, name, value );
   db.collection(COLLECTION).insertOne( result, function( err, writeResult ) {
      cb( err, transformThing( writeResult.ops[0] ) );
   } );
};
module.exports.create = create;

function find( ownerId, thingId, cb ) {
   db.collection(COLLECTION).findOne( { ownerId : ownerId, '_id' : new mongo.ObjectID(thingId) }, function(err, thing ) {
      cb( err, transformThing( thing ) );
   } );   
};
module.exports.find = find;

function findByOwner( ownerId, cb ) {
  db.collection(COLLECTION).find( {ownerId : ownerId} ).toArray( function(err, things) {
    cb( err, things.map( transformThing ) ); 
  } );
};
module.exports.findByOwner = findByOwner;

function deleteThing( uid, tid, cb ) {
   find( uid, tid, function( error, thing ) {
      if( error ) {
         cb( error, null );
      } else {
         db.collection(COLLECTION).deleteOne( { '_id' : new mongo.ObjectID(tid) }, function( removeError, info ) {
            cb( removeError, thing );
         } );
      }
   } );
};
module.exports.deleteThing = deleteThing;

function update( uid, tid, newThing, cb ) {
   find( uid, tid, function( error, thing ) {
      if( error ) {
         cb( error );
      } else {
         newThing = { name : newThing.name || thing.name,
                      value : newThing.value || thing.value };
         db.collection(COLLECTION).update(  { '_id' : new mongo.ObjectID(tid) }, {$set : newThing}, function(err, updated ) {
            find( uid, tid, cb );
         } );
      }
   });
};
module.exports.update = update;
