/* user is an object of type
 * { name : { first : <string>, last : <string> },
 *   email : <string>,
 *   enabled : <boolean>,
 *   password : <string>,
 *   id : <string>
 * }
 */ 
function User( first, last, email, password, enabled ) {
   this.name = { first : first, last : last },
   this.email = email;
   this.password = password;
   this.enabled = enabled;
}

module.exports = User;
