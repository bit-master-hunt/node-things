var MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect("mongodb://localhost:27017/things",  { useNewUrlParser: true, useUnifiedTopology: true}, (err, client ) => {
  if(err) throw err;
  db = client.db( );

});


module.exports = { collection : (name) => db.collection(name) }

