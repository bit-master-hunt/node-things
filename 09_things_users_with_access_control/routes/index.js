var express = require('express');
var router = express.Router();
var uuid = require('uuid');
var users = require('./users');

function Thing( ownerId, name, value ) {
   this.id = uuid.v4();
   this.ownerId = ownerId;
   this.name = name;
   this.value = value;
};

// This is a collection of things
//   indexed by thing id
//   each value is a thing
var thingsDb = {};

function isOwner( ownerId, thingId ) {
   var thing = thingsDb[ thingId ];
   return thing && thing.ownerId == ownerId;
}

var createThing = function( ownerId, name, value ) {
   var result = new Thing( ownerId, name, value );
   thingsDb[ result.id ] = result;
   return result;
};

var deleteThing = function( tid ) {
   var result = thingsDb[ tid ];
   delete thingsDb[ tid ];
   return result;
};

var updateThing = function( ownerId, tid, newThing ) {
   var oldThing = thingsDb[ tid ];
   if( oldThing ) {
      oldThing.name = newThing.name || oldThing.name;
      oldThing.value = newThing.value || oldThing.value;
   }

   return oldThing;
};

function contains( text, key ) {
   return text.toUpperCase().indexOf( key.toUpperCase() ) >= 0;
};

/**************** ROUTES ***********************************/

router.all( '/users/:uid/*', function( req, res, next ) {
   var pathUser = users.findById( req.params.uid );
   var authenticatedUser = req.session.user;
   console.log( req.params.uid, pathUser, authenticatedUser );
   if( authenticatedUser && pathUser && authenticatedUser.id == pathUser.id ) {
      next();
   } else {
      res.redirect( '/' );
   }
} );

router.get('/users/:uid/things', function( req, res, next ) {
   var filter = req.query.filter || '';
   var result = [];
   for( var key in thingsDb ) {
      result.push( thingsDb[ key ] );
   }

   result = result.filter( thing => thing.ownerId == req.params.uid );
   
   if( filter ) {
      result = result.filter( thing => (
         thing.id==filter || contains( thing.name, filter ) || contains( thing.value, filter )
      ) );
   }
   res.send( result );
} );

router.get('/users/:uid/things/:tid', function( req, res, next ) {
   var result = thingsDb[ req.params.tid ] || { status : 'no such thing' };
   if( result.ownerId == req.params.uid ) {
      res.json( result );
   } else {
      res.status( 404 ).send( result );
   }
} );

router.post('/users/:uid/things', function( req, res, next ) {
   var result = createThing( req.params.uid, req.body.name, req.body.value );
   res.send( result );
} );

router.delete('/users/:uid/things/:tid', function( req, res, next ) {
   var result = deleteThing( req.params.uid, req.params.tid );
   res.send( result );
} );

router.put('/users/:uid/things/:tid', function( req, res, next ) {
   var result = updateThing( req.params.uid, req.params.tid, req.body );
   res.send( result );
} );

module.exports = router;