const uuid = require('uuid');

/*
 * userDb is an object having
 * user.emails as keys
 * and user objects as values
 */
const userDb = [["bilbo", "baggins"], ["frodo", "baggins"], ["samwise", "gamgee"], ["gandalf", "gray"]]
      .reduce( (acc,cv) => {
         var user = new User(cv[0], cv[1], cv[0] + "@mordor.org", "123", true);
         acc[user.email] = user;
         return acc;
      }, {} );

console.log( userDb );

/* user is an object of type
 * { name : { first : <string>, last : <string> },
 *   email : <string>,
 *   enabled : <boolean>,
 *   password : <string>,
 *   id : <string>
 * }
 */ 
function User( first, last, email, password, enabled, id ) {
   this.id = id || uuid.v4();
   this.name = { first : first, last : last },
   this.email = email;
   this.password = password;
   this.enabled = enabled;
}

function findAll( ) {
   var users = [];
   for( var id in userDb ) {
      users.push(id);
   }
   return users;
}
module.exports.findAll = findAll;

function findById( id ) {
   for( var userId in userDb ) {
      var user = userDb[ userId ];
      if( user.id == id ) return user;
   }
   
   return null;
}
module.exports.findById = findById;

function findByEmail( email ) {
   var user = userDb[ email ];
   return user && { name : user.name,
                    email : user.email,
                    enabled : user.enabled,
                    id : user.id,
                    password : user.password
                  };
}

module.exports.findByEmail = findByEmail;



