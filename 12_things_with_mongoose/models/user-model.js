var mongoose = require('mongoose');

/* user is an object of type
 * { name : { first : <string>, last : <string> },
 *   email : <string>,
 *   enabled : <boolean>,
 *   password : <string>,
 *   id : <string>
 * }
 */ 
var userSchema = mongoose.Schema({
        name : { 
            first : String, last : String
        },
        email : String,
        password : String,
        enabled : Boolean
} );

userSchema.set('toJSON', {
   transform : function( doc, result, options ) {
      result.id = result._id;
      delete result._id; // mongo internals
      delete result.__v; // mongo internals
   }
} );


var User = mongoose.model('User', userSchema );
module.exports = User;
