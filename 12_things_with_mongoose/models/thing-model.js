var mongoose = require('mongoose');

var thingSchema = mongoose.Schema( {
        owner : mongoose.Schema.ObjectId,
        name : String,
        value : String
});

thingSchema.set('toJSON', {
   transform : function( doc, result, options ) {
      result.id = result._id;
      delete result._id; // mongo internals
      delete result.__v; // mongo internals
   }
} );

var Thing = mongoose.model('Thing', thingSchema );

module.exports = Thing;
