var Thing = require('../models/thing-model');

function create( ownerId, name, value, cb ) {
   new Thing( { owner : ownerId, name : name, value : value } ).save( cb );
};
module.exports.create = create;

function findById( tid, cb ) {
   Thing.findById( tid, cb );
};
module.exports.find = findById;

function findByOwner( uid, filter, cb ) {
   var query = { "owner" : uid };

   if( filter ) {
      var containsRegEx = ".*"+ filter + ".*";
      query.$or = [ 
         { 'name' : { $regex : containsRegEx } },
         { 'value' : { $regex : containsRegEx}  }
      ];

      if( filter.length == 24 ) {
         query.$or.push( { '_id' : filter } );
      }
   }

   Thing.find( query, cb );
};
module.exports.findByOwner = findByOwner;

function deleteThing( uid, tid, cb ) {
   Thing.findOneAndRemove( { owner : uid, '_id' : tid }, cb );
};
module.exports.deleteThing = deleteThing;

function update( uid, tid, newThing, cb ) {
   Thing.findOne( { owner : uid, '_id' : tid }, function( err, thing ) {
      if( err ) { cb( err, null ); }
      else {
         thing.value = newThing.value;
         thing.name = newThing.name;
         thing.save( cb );
      }
   } );
};
module.exports.update = update;
