import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {Routes, RouterModule, Router} from "@angular/router";

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ThingsListComponent } from './things-list/things-list.component';
import { ThingComponent } from './thing/thing.component';
import { ThingsListMenuComponent } from './things-list-menu/things-list-menu.component';
import { UserServiceService } from './user-service.service';
import { ThingCreateComponent } from './thing-create/thing-create.component';
import { ThingUpdateComponent } from './thing-update/thing-update.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent },
  {path: 'home', component: ThingsListComponent },
  {path: 'home/things/:thingid', component : ThingUpdateComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ThingsListComponent,
    ThingComponent,
    ThingsListMenuComponent,
    ThingCreateComponent,
    ThingUpdateComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot( routes, { useHash : true })
  ],
  providers: [HttpClientModule, UserServiceService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
