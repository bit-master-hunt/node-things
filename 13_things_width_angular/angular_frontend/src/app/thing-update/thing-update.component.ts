import { Component, OnInit, Input } from '@angular/core';
import {Router, ActivatedRoute } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';



@Component({
  selector: 'app-thing-update',
  templateUrl: './thing-update.component.html',
  styleUrls: ['./thing-update.component.css']
})
export class ThingUpdateComponent implements OnInit {
  @Input() thing : any = { name :'', value : ''};
  private thingId : string;

  constructor(private router : Router, private http : HttpClient, private route : ActivatedRoute, private userService : UserServiceService ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.thingId = params['thingid'];
      var THING_URL = `http://localhost:3000/api/users/${this.userService.getUser().id}/things/${this.thingId}`;
      this.http.get( THING_URL, {withCredentials : true } ).subscribe(
        thing => {
          this.thing = thing;
        }
      );      
    });
  }

  cancel() {
    this.router.navigate(['home']);
  }

  deleteThing( ) {
    var THINGS_URL = `http://localhost:3000/api/users/${this.userService.getUser().id}/things/${this.thing.id}`;
    this.http.delete( THINGS_URL, {withCredentials : true } ).subscribe(
      data => {
        this.cancel();
      }
    );
  }


  updateThing( ) {
    var THINGS_URL = `http://localhost:3000/api/users/${this.userService.getUser().id}/things/${this.thing.id}`;
    this.http.put( THINGS_URL, this.thing, {withCredentials : true } ).subscribe(
      data => {
        this.thing = data;
      }
    );
  }
}
