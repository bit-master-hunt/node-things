import { Component, OnInit, Input, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { Observable } from 'rxjs/Rx'; 
import "rxjs/add/operator/map";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@Injectable()
export class LoginComponent implements OnInit {
  @Input() username : string;
  @Input() password : string;
  LOGIN_URL : string = "http://localhost:3000/login";
  
  constructor(private http : HttpClient, private router : Router, private userService : UserServiceService ) { }


  ngOnInit() {
    this.username = "bilbo@mordor.org";
    this.password = "123";
   }
  
  login() {
    var credentials = { username : this.username, password : this.password };
    this.http.post( this.LOGIN_URL, credentials, { observe : 'response'} )
      .map( res => {
        this.userService.setToken( res.headers.get('X-CSRF-TOKEN') );
        return res.body;
      })
      .subscribe( 
        (data) => {
          this.userService.setUser( data );                
          this.router.navigateByUrl( 'home');
      }
    );
  }

}
