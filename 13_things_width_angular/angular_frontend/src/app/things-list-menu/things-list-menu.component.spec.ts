import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThingsListMenuComponent } from './things-list-menu.component';

describe('ThingsListMenuComponent', () => {
  let component: ThingsListMenuComponent;
  let fixture: ComponentFixture<ThingsListMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThingsListMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThingsListMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
