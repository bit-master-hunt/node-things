import { Component, OnInit, Input, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import {Router } from "@angular/router";



@Component({
  selector: 'app-things-list',
  templateUrl: './things-list.component.html',
  styleUrls: ['./things-list.component.css']
})
@Injectable()
export class ThingsListComponent implements OnInit {
  @Input() things : any;
  filter : string;

  constructor(private http : HttpClient, private userService : UserServiceService, private router : Router) { }

  ngOnInit() {
    this.retrieveThings();
  }

  retrieveThings() {
    this.filter = '';
    this.searchThings();
  }

  searchThings() {
    var THINGS_URL = `http://localhost:3000/api/users/${this.userService.getUser().id}/things/`;
    this.http.get( THINGS_URL, { params : { filter : this.filter }, withCredentials : true } ).subscribe(
      data => {
        this.things = data;
      }
    ); 
    this.filter = '';
  }

  deleteThing( thing : any ) {
    var THINGS_URL = `http://localhost:3000/api/users/${this.userService.getUser().id}/things/${thing.id}`;
    this.http.delete( THINGS_URL, {withCredentials : true } ).subscribe(
      data => {
        this.things = this.things.filter( t => t.id != thing.id );
      }
    );
  }

  thingCreate( thing : any ) {
    this.things.unshift( thing );
  }

  logout() {
    var LOGOUT_URL = 'http://localhost:3000/logout';
    this.http.post( LOGOUT_URL, {}, {withCredentials : true } ).subscribe(
      data => {
        this.router.navigate(['login']);
      }
    )
  }

}
