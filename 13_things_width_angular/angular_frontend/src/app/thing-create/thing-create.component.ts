import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../user-service.service';
import { Observable } from 'rxjs/Rx'; 
import "rxjs/add/operator/map";


@Component({
  selector: 'app-thing-create',
  templateUrl: './thing-create.component.html',
  styleUrls: ['./thing-create.component.css']
})
export class ThingCreateComponent implements OnInit {
  newThing : any = { name : '', value : '' };
  @Output() onThingCreate = new EventEmitter<any>();

  constructor(private http : HttpClient, private userService : UserServiceService ) {}

  ngOnInit() {}


  thingCreate() {
    let URL = `http://localhost:3000/api/users/${this.userService.getUser().id}/things`;
    this.http.post( URL, this.newThing, { observe : 'response'} )
      .map( res => {
        this.userService.setToken( res.headers.get('X-CSRF-TOKEN') );
        return res.body;
      })
      .subscribe( 
        data => {
          console.log( "thingCreate: ", data );
          this.onThingCreate.emit( data );
          this.newThing.name = '';
          this.newThing.value = '';
        }
    );
  }
}
