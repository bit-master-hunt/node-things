var express = require('express');
var router = express.Router();
var users = require('./users');
var things = require('./things');

function isOwner( ownerId, thingId ) {
   var thing = thingsDb[ thingId ];
   return thing && thing.ownerId == ownerId;
}

function contains( text, key ) {
   return text.toUpperCase().indexOf( key.toUpperCase() ) >= 0;
};

/**************** ROUTES ***********************************/
// put this one first to avoid authentication concerns
router.get('/users/init', function( req, res, next ) {
   users.init( (err, result) => res.json( result ) );
} );

router.all( '/users/:uid/*', function( req, res, next ) {
   users.findById( req.params.uid, function( error, pathUser ) {
      var authenticatedUser = req.session.user;
      if( authenticatedUser && pathUser && authenticatedUser.id == pathUser.id ) {
         next();
      } else {
          console.log( authenticatedUser, pathUser );
         res.redirect( '/' );
      }
   } );
} );

router.get('/users', function( req, res, next  ) {
   users.findAll( function(error, result) {
      if( error ) {
         res.status(500).json( error);
      } else {
         res.json( result );
      }
   } );
} );

router.get('/users/:uid/things', function( req, res, next ) {
   var filter = req.query.filter || '';
   things.findByOwner( req.params.uid, function( err, result ) {
      if( filter ) {
         result = result.filter( thing => (
            thing.id==filter || contains( thing.name, filter ) || contains( thing.value, filter )
         ) );
      }
      res.json( result );
   } );
} );

router.get('/users/:uid/things/:tid', function( req, res, next ) {
   things.find( req.params.uid, req.params.tid, function( err, thing ) {
      if( thing ) {
         res.json( thing );
      } else {
         res.status( 404 ).send( 'no such thing' );
      }
   } );
} );
           
router.post('/users/:uid/things', function( req, res, next ) {
   things.create( req.params.uid, req.body.name, req.body.value, function( err, thing ) {
      if( err ) {
         res.status( 500 ).send( { 'msg' : 'Error creating thing' } );
      } else {
         res.send( thing );
      }
   } );
} );

router.delete('/users/:uid/things/:tid', function( req, res, next ) {
   things.deleteThing( req.params.uid, req.params.tid, function( err, result ) {
      if( err ) {
         res.status( 500 ).send( { msg : `error deleting ${result.id}` } );
      } else {
         res.json( result );
      }
   } );
} );

router.put('/users/:uid/things/:tid', function( req, res, next ) {
   things.update( req.params.uid, req.params.tid, req.body, function( err, thing ) {
      if( err ) {
         res.status( 403 ).json( { msg : 'error' } );
      } else {
         res.json( thing );
      }
   } );
} );

module.exports = router;
