function xHeader( ) {
    var headers = [];
    for( var i = 0; i < arguments.length; i++) {
        headers.push( arguments[i] );
    }
    
    return function( req, res, next ) {
        headers.forEach( h => res.removeHeader( h ) );
        next();
    }
}

module.exports = xHeader;

