function logger( header ) {
   
   return function( req, res, next ) {
      var message = [Date.now(), req.method, req.protocol, req.path].join('--');
      if( header ) {
         res.set( header, message );
      } else {
         console.log( message );
      }

      next();
   }
}

module.exports = logger;
