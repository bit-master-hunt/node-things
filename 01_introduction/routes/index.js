var express = require('express');
var router = express.Router();
var logger = require('./logger');
var xheader = require('./x-header');

router.all( '*', xheader('x-powered-by', 'date' ) );
router.all( '*', logger('x-debug') );

router.get('/', function( req, res, next ) {
   res.json( 'Hello World' );
} );

module.exports = router;
