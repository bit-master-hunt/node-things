// Global state variable(s)
var modalState = {
   thing : null
};

/************************************** AJAX  *****************************/
function createThing( ) {
   var name = $('#name').val();
   var value = $('#value').val();

   $('#name').val('');
   $('#value').val('');
   
   $.ajax( {
      url : 'api/things',
      method : 'POST',
      data : { name : name, value : value },
      success : retrieveThings
   } );
};

function retrieveThings( ) {
   $.ajax( {
      url : 'api/things',
      method : 'GET',
      success : updateTable
   } );
};

function updateThing( ) {
   var newName = $('#nameModal').val();
   var newValue= $('#valueModal').val();
   
   $.ajax( {
      url : 'api/things/' + modalState.thing.id,
      method : 'PUT',
      success : ( updatedThing ) => { updateRow( modalState.thing.row, updatedThing ); showModal( null ); },
      data : { id : modalState.thing.id, name : newName, value : newValue }
   } );
}

function deleteThing( ) {
   var thing = modalState.thing;
   $.ajax( {
      url : 'api/things/' + thing.id,
      method : 'DELETE',
      success : (thing) => { retrieveThings(); showModal( null ); }
   } );
}

function searchThings( event ) {
   event.preventDefault();
   
   var filter = $('#search').val();
   $('#search').val('');

   if( !filter ) return;
   
   $.ajax( {
      url : 'api/things/',
      method : 'GET',
      data : { filter : filter },
      success : updateTable
   } );
};
