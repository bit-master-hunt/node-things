var details = angular.module('Things.Detail', ['ngRoute', 'ngResource']);

details.controller('Things.Thing.Controller', ['$scope', '$routeParams', '$resource', '$location', function($scope, $routeParams, $resource, $location) {
   var Things = $resource('/things/api/things/:tid', null, { update : { method : 'PUT' } } );
   $scope.thing = Things.get( { tid : $routeParams['tid'] }, null)

   $scope.updateThing = function() {
      Things.update( { tid : $routeParams[ 'tid' ] },
                     $scope.thing,
                     function( result ) {
                        $location.path('#/things/' + $scope.thing.id);
                     }
                   );
   };

   $scope.cancel = function() {
      $location.path( "/things" );
   };

   $scope.deleteThing = function() {
      Things.delete( { tid : $routeParams[ 'tid' ] },
                     function( result ) {
                        $location.path('#/things/' + $scope.thing.id);
                     }
                   );
      
   }
}]);
