var things = angular.module('Things.ThingList', ['ngRoute', 'ngResource'] );

things.controller('Things.ThingList.Controller', ['$scope', '$resource', '$location', function($scope, $resource, $location ) {
   var Things = $resource('things/api/things/:tid', {});
   
   $scope.things = Things.query( );
   $scope.newThing = { name : '', value : '' };
   $scope.filter = '';
   
   $scope.delete = function( thing ) {
      Things.delete( { tid : thing.id }, function() {
         $scope.things = $scope.things.filter( function( t ) { return t.id !== thing.id; } );
      } )
   }

   $scope.selectThing = function( thing ) {
      console.log( '#/things/' + thing.id );
      $location.path( '#/things/' + thing.id );
   }

   $scope.retrieveThings = function() {
      $scope.things = Things.query();
   }

   $scope.createThing = function() {
      Things.save( {}, 
                   $scope.newThing,
                   function( response ) {
                      $scope.newThing = { name : '', value : '' },
                      $scope.things.push( response );
                   }
                 );
   };

   $scope.searchThings = function() {
      $scope.things = Things.query( { filter : $scope.filter } );
      $scope.filter = '';
   }
}]);
