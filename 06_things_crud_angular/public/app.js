var app = angular.module('Things.App', [
   'ngRoute',
   'Things.Detail',
   'Things.ThingList'
]);

app.controller('Things.Controller', function() {
} );

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider ) {
   $locationProvider.html5Mode( true );
      
   $routeProvider.when('/things', {
      templateUrl: 'views/things.html',
      controller: 'Things.ThingList.Controller'
   } ).when('/things/:tid', {
      templateUrl: 'views/thing.html',
      controller: 'Things.Thing.Controller'
   }).otherwise( {
      redirectTo: 'things'
   });
   
}]);
