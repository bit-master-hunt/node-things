// Global state variable
var state = {
   modal : { thing : null },
   page : { pages : ["login", "content", "editModal" ], page : null },
   user : null,
}

function getCSRF() {
	function getCookie(name) {
	  var value = "; " + document.cookie;
	  var parts = value.split("; " + name + "=");
	  if (parts.length == 2) return parts.pop().split(";").shift();
	}
	
	var cookie = getCookie('XSRF-TOKEN');
	return cookie || null;
}

function getHeaders() {
	return { "X-XSRF-TOKEN" : getCSRF() };
}


/********************************** DOM FUNCTIONS ****************************************/
$(document).ready( function() {
	$.ajax( {
      url : '/user',
      method : 'GET',
      headers : getHeaders(),
      success : (user)=>{ $('body').show().addClass('background'); setUser(user); },
      error : () => { $('body').show().addClass('background'); setPage( 'login' ) ; }
    });
});

function updateFake() {
	function updateFakeThing( fake ) {
		if( fake ) {
			$('#fake-name').text( fake.name );
			$('#fake-value').text( JSON.stringify(JSON.parse(fake.value), null, 2) );
		}
	}
	
	$.ajax( {
	      url : '/fake',
	      method : 'GET',
	      headers : getHeaders(),
	      success : updateFakeThing
	    });
}

function setPage( page ) {
   state.page.page = page;
   if( page == 'login' ) {
      $('body').addClass( 'background' );
   } else {
      $('body').removeClass('background');
   }

   if( page == 'content' ) {
      retrieveThings();
   }
   
   state.page.pages.forEach(
      p => {
         var selector = "#" + p;
         state.page.page == p ? $(selector).show() : $(selector).hide();
      }
   );
}

function setUser( user ) {
   state.user = user;
   console.log( state );
   $('#email').text( user && user.username );
   setPage( user ? 'content' : 'login' );
}

function login(evt) {
   evt.preventDefault();
   var password = $('#login_password').val();
   var username = $('#login_username').val();

   $('#login_password').val('');
   $('#login_username').val('');
   
   $.ajax( {
      url : '/login',
      headers : getHeaders(),
      data :  { "username" : username, "password" : password },
      method : 'POST',
      success : setUser
   } );
}

function logout(evt) {
   $.ajax( {
      url : '/logout',
      headers : getHeaders(),      
      method : 'POST',
      success : () => setUser(null)
   } );
}

function makeRow( type, values ) {
   return $(`<tr><${type}>` + values.join(`</${type}><${type}>`) + `</${type}></${type}>` );
}

function updateRow( row, thing ) {
   var props = ['id', 'name', 'value' ];
   var newRow = makeRow( 'td', props.map( p => thing[p] ) );
   newRow.click( (event) => showModal( thing ) );   
   row.replaceWith( newRow );
   thing.row = newRow;
}

function updateTable( things ) {
	updateFake();
   var table = $('#table').empty();
   var props = ['id', 'name', 'value' ];

   // make header
   makeRow( 'th', props ).appendTo( table );

   things.forEach( thing => {
      var tr = makeRow( 'td', props.map( p => thing[p] ) );
      tr.click( (event) => showModal( thing ) );
      tr.appendTo( table );
      thing.row = tr;
   } );
};

function showModal( thing ) {
   state.modal.thing = thing;
   if( thing ) {
      $('#nameModal').val( thing.name );
      $('#valueModal').val( thing.value );
      $('#editModal').slideDown();
      $('#content').slideUp();
   } else {
      $('#editModal').slideUp();
      $('#content').slideDown();
   }
}

function cancel() {
   showModal( null );
}


/************************************** AJAX  *****************************/
function createThing( evt ) {
	evt.preventDefault();
   var name = $('#name').val();
   var value = $('#value').val();

   $('#name').val('');
   $('#value').val('');
   
   $.ajax( {
      url : `api/users/${state.user.username}/things`,
      method : 'POST',
      headers : getHeaders(),      
      contentType : 'application/json',
      data : JSON.stringify( { name : name, value : value } ),
      success : retrieveThings
   } );
};

function retrieveThings( ) {
   $.ajax( {
      url : `api/users/${state.user.username}/things`,
      headers : getHeaders(),      
      method : 'GET',
      success : updateTable
   } );
};

function updateThing( ) {
   var newName = $('#nameModal').val();
   var newValue= $('#valueModal').val();
   
   $.ajax( {
      url : `api/users/${state.user.username}/things/` + state.modal.thing.id,
      method : 'PUT',
      contentType : 'application/json',
      headers : getHeaders(),      
      success : ( updatedThing ) => { updateRow( state.modal.thing.row, updatedThing ); showModal( null ); },
      data : JSON.stringify( { id : state.modal.thing.id, name : newName, value : newValue } )
   } );
}

function deleteThing( ) {
   var thing = state.modal.thing;
   $.ajax( {
      url : `api/users/${state.user.username}/things/` + thing.id,
      method : 'DELETE',
      headers : getHeaders(),      
      success : (thing) => { retrieveThings(); showModal( null ); }
   } );
}

function searchThings( event ) {
   event.preventDefault();
   
   var filter = $('#search').val();
   $('#search').val('');

   if( !filter ) return;
   
   $.ajax( {
      url : `api/users/${state.user.username}/things/`,
      method : 'GET',
      headers : getHeaders(),      
      data : { filter : filter },
      success : updateTable
   } );
};
