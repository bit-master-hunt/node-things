package com.hunt.things.controllers;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.models.Thing;

@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "false")
@RestController
public class FakeController {
	private String getSWAPI() throws URISyntaxException, ClientProtocolException, IOException {
		int id = (int)(Math.random() * 30 + 1 );
		URI uri = new URIBuilder()
		        .setScheme("https")
		        .setHost("swapi.co")
		        .setPath("/api/people/" + id)		        
		        .build();
		HttpGet httpget = new HttpGet(uri);
		
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = client.execute( httpget );
		
		String result = null;
		try {
			HttpEntity e = response.getEntity();
			result = EntityUtils.toString( e );			
		} finally {
			response.close();
		}
		return result;
	}
	
	@RequestMapping(value="/fake", method=RequestMethod.GET)
	public Thing fakeThing( ) throws ClientProtocolException, URISyntaxException, IOException {
		String value = getSWAPI();
		return new Thing.Builder().name("swapi").value(value).owner("Shell Oil").build();
	}
}
