package com.hunt.things.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.hunt.things.models.Thing;

public interface ThingsRepository extends MongoRepository<Thing, String> {
	@Query("{ $and : [ {'owner' : ?0}, { $or:[ {'name':{ $regex : ?1 }}, {'value':{ $regex : ?1 } } ] } ] }")
	List<Thing> findThings(String uname, String filter);
	
	List<Thing> findByOwner( String owner );
}
