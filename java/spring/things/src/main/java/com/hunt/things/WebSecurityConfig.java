package com.hunt.things;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import com.hunt.things.security.AuthenticationFailure;
import com.hunt.things.security.AuthenticationSuccess;
import com.hunt.things.security.EntryPointUnauthorizedHandler;
import com.hunt.things.services.ThingsUserService;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private ThingsUserService service;
	
	@Autowired
	private AuthenticationFailure authFailure;
	
	@Autowired
	private AuthenticationSuccess authSuccess;

	@Autowired
	private EntryPointUnauthorizedHandler authDenied;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            		.antMatchers("/", "/css/**", "/assets/**", "/js/**", "/user", "/login", "/logout").permitAll()
                .anyRequest().authenticated()
                .and()
            .exceptionHandling()
            		.authenticationEntryPoint(authDenied)
            		.and()
            .formLogin()
				.successHandler(authSuccess)
				.failureHandler(authFailure)
				.loginPage("/")
				.loginProcessingUrl("/login")
				.defaultSuccessUrl("/user")
				.usernameParameter("username")
				.passwordParameter("password")
				.permitAll()
                .and()
            .logout()
                .permitAll()
                .and()
            .csrf()            		
            		.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }

    @Autowired
    public void configureGlobal( AuthenticationManagerBuilder auth ) throws Exception {
    		auth.userDetailsService( service );
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider( authenticationProvider() );
    }
    
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(service);
        authProvider.setPasswordEncoder(encoder());
        
        return authProvider;
    }
    
    @Bean
    public PasswordEncoder encoder() {
    		PasswordEncoder encoder = new BCryptPasswordEncoder(11);
        return encoder;
    }
}