package com.hunt.things.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.models.Thing;
import com.hunt.things.security.BadRequestException;
import com.hunt.things.services.ThingsService;

@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "false")
@RestController
@RequestMapping( "/api/users/{uname}/things" )
public class ThingsController {
	@Autowired
	private ThingsService thingsService;
	
	private void validateOwner( Principal p, String uname ) throws BadRequestException {
		if( p == null || !p.getName().equals( uname ) ) throw new BadRequestException(); 
	}
	
	private void validateThingOwner( String tid, String uname ) throws BadRequestException {
		Thing tOld = thingsService.findById( tid );		
		if( !tOld.getOwner().equals(uname) ) throw new BadRequestException();
	}

	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Thing> findAll( Principal p, @PathVariable String uname, @RequestParam(required=false) String filter ) throws BadRequestException {
		validateOwner( p, uname );
		return thingsService.find(uname, filter);
	}
	
	@RequestMapping(value="/{tid}", method=RequestMethod.GET)
	public Thing find(Principal p, @PathVariable String uname, @PathVariable String tid) throws BadRequestException {
		validateOwner( p, uname );
		validateThingOwner( tid, uname );
		return thingsService.findById( tid );
	}
	
	@RequestMapping(value="/{tid}", method=RequestMethod.DELETE)
	public Thing deleteThing(Principal p, @PathVariable String uname, @PathVariable String tid ) throws BadRequestException {
		validateOwner( p, uname );
		validateThingOwner( tid, uname );		
		return thingsService.deleteById( tid );
	}
	
	@RequestMapping(value="/{tid}", method=RequestMethod.PUT)
	public Thing updateThing(Principal p, @PathVariable String uname, @PathVariable String tid, @RequestBody Thing t ) throws BadRequestException {
		validateOwner( p, uname );
		validateThingOwner( tid, uname );
		if( t.getId().equals(tid) ) {
			return thingsService.updateThing( t );
		} else {
			throw new BadRequestException();
		}
	}

	@RequestMapping(value="", method=RequestMethod.POST)
	public Thing createThing( @RequestBody Thing t, Principal p, @PathVariable String uname ) throws BadRequestException {
		validateOwner( p, uname );
		t.setOwner( uname );
		return thingsService.createThing( t );
	}

}