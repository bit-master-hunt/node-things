package com.hunt.things.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.things.models.Thing;
import com.hunt.things.repositories.ThingsRepository;

@Service
public class ThingsService {
	@Autowired
	private ThingsRepository thingsRepository;

	public List<Thing> find(String userName, String filter) {	
		if( filter == null ) {
			return thingsRepository.findByOwner( userName );
		} else {
			return thingsRepository.findThings(userName, filter);			
		}
	}

	public Thing findById(String tid) {
		return thingsRepository.findById( tid ).get();
	}

	public Thing deleteById(String id) {
		Thing oldThing = findById( id );
		thingsRepository.delete( oldThing );
		return oldThing;
	}

	public Thing createThing(Thing t) {
		return thingsRepository.save(t);
	}
	
	public Thing updateThing( Thing t ) {
		return thingsRepository.save( t );
	}
}
