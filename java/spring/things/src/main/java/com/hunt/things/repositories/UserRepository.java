package com.hunt.things.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hunt.things.models.ThingUser;

public interface UserRepository extends MongoRepository<ThingUser, String> {
	public ThingUser findByUsername( String username );
}
