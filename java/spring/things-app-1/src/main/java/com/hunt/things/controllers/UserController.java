package com.hunt.things.controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.exceptions.BadRequestException;
import com.hunt.things.models.Thing;
import com.hunt.things.models.User;
import com.hunt.things.services.ThingService;
import com.hunt.things.services.UserService;

@RestController
@RequestMapping( "/api/v1/users" )
public class UserController {
	@Autowired
	private UserService userService;

	@Autowired
	private ThingService thingService;



	@RequestMapping( value = "", method = RequestMethod.GET )
	public Collection<User> getUsers() {
		return userService.findAll();
	}

	@RequestMapping( value = "/{id}", method = RequestMethod.GET )
	public User getUser( @PathVariable String id ) {
		return userService.findOne( id );
	}

	@RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
	public User deleteThing( @PathVariable String id ) {
		return userService.deleteUser( id );
	}

	@RequestMapping( value = "", method = RequestMethod.POST ) 
	public User createThing( @RequestBody User user ) {
		return userService.createUser( user );
	}

	@RequestMapping( value = "/{id}", method = RequestMethod.PUT )
	public User updateUser( @RequestBody User user, @PathVariable String id ) throws BadRequestException {
		return userService.updateUser( user );
	}


	private String randomWord() {
		int length = (int)(Math.random() * 10 + 5 );
		final String letters = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder result = new StringBuilder();
		while( result.length() < length ) {
			int index = (int)(Math.random() * letters.length() );
			result.append( letters.substring( index,  index+1 ) );
		}
		return result.toString();
	}

	@PostConstruct
	private void init() {
		if( userService.findAll().isEmpty() ) {
			List<String> firsts = Arrays.asList("Kenny", "Karen", "Deantye", "Tyrell", "Nathaniel", "Rebekah", "Hannah" );
			List<String> lasts = Arrays.asList("Hunt", "Smith", "Jones", "William", "Hondo", "Burka", "Daja" );
			IntStream
			.range(0, firsts.size())
			.mapToObj( i -> new User.Builder()
					.first( firsts.get(i) )
					.last( lasts.get(i) )
					.email( String.format("%s.%s@acme.org", firsts.get(i), lasts.get(i) ) )
					.build() )
			.forEach( user -> userService.createUser( user ) );

			userService
			.findAll()
			.stream()
			.forEach( user -> {
				IntStream
				.range( 0,  (int)(Math.random() * 10 ) )
				.forEach( i -> {
					thingService.createThing( new Thing.Builder()
							.name( randomWord() )
							.value( randomWord() )
							.owner( user.getId() )
							.build() );
				} );
			});
		}
	}
}
