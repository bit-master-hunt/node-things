package com.hunt.things.services;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.things.exceptions.BadRequestException;
import com.hunt.things.models.Thing;
import com.hunt.things.models.User;
import com.hunt.things.repositories.ThingRepository;

@Service
public class ThingService {
	@Autowired
	private ThingRepository thingRepository;
	
	public Collection<Thing> findAll() {
		return thingRepository.findAll();
	}

	public Thing findOne(String id) {
		return thingRepository.findById( id ).get();
	}

	public Thing deleteThing(String id) {
		Thing toDelete = findOne( id );
		thingRepository.delete( toDelete );
		return toDelete;
	}

	public Thing createThing( Thing thing ) {
		return thingRepository.save(thing);
	}
	
	public Thing updateThing(Thing thing) throws BadRequestException {
		Thing old = findOne( thing.getId());
		if( old != null ) {
			return thingRepository.save( thing );
		} else {
			throw new BadRequestException();
		}
	}
	
	public List<Thing> findByOwner( User user ) {
		return thingRepository.findByOwner( user.getId() );
	}

}
