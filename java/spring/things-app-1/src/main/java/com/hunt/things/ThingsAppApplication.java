package com.hunt.things;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThingsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThingsAppApplication.class, args);
	}
}
