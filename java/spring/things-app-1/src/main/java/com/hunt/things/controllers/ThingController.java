package com.hunt.things.controllers;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.exceptions.BadRequestException;
import com.hunt.things.models.Thing;
import com.hunt.things.models.User;
import com.hunt.things.services.ThingService;
import com.hunt.things.services.UserService;

@RestController
@RequestMapping( "/api/v1/users/{uid}/things" )
public class ThingController {
	@Autowired
	private ThingService thingService;
	
	@Autowired
	private UserService userService;

	private User validateUser( String id ) throws BadRequestException {
		User result = userService.findOne(id);
		if( result == null ) {
			throw new BadRequestException();
		} else {
			return result;
		}
	}
	
	@RequestMapping( value = "", method = RequestMethod.GET )
	public Collection<Thing> getThings( @PathVariable String uid ) throws BadRequestException {		
		return thingService.findByOwner( validateUser( uid ) );
	}
	
	@RequestMapping( value = "/{id}", method = RequestMethod.GET )
	public Thing getThing( @PathVariable String uid, @PathVariable String id ) throws BadRequestException {
		validateUser( uid );
		return thingService.findOne( id );
	}
	
	@RequestMapping( value = "/{id}", method = RequestMethod.DELETE )
	public Thing deleteThing( @PathVariable String uid, @PathVariable String id ) throws BadRequestException {
		validateUser( uid );
		return thingService.deleteThing( id );
	}
	
	@RequestMapping( value = "", method = RequestMethod.POST ) 
	public Thing createThing( @PathVariable String uid, @RequestBody Thing thing ) throws BadRequestException {
		validateUser( uid );
		User user = userService.findOne( thing.getOwner() );
		if( user != null ) {
			return thingService.createThing( thing );
		} else throw new BadRequestException();
	}
	
	@RequestMapping( value = "/{id}", method = RequestMethod.PUT )
	public Thing updateThing( @PathVariable String uid, @RequestBody Thing thing, @PathVariable String id ) throws BadRequestException {
		validateUser( uid );
		return thingService.updateThing( thing );
	}
}
