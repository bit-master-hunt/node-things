package com.hunt.things.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Thing {
	private String name;
	private String value;
	private String owner;
	@Id
	private String id;
	
	public Thing() {		
	}
	
	public Thing( Builder b ) {
		this.name = b.name;
		this.owner = b.owner;
		this.value = b.value;
		this.id = b.id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public static class Builder {
		private String id;
		private String name;
		private String value;
		private String owner;
		
		public Builder id( String id ) {
			this.id = id;
			return this;
		}
		
		public Builder name( String name ) {
			this.name = name;
			return this;
		}
		
		public Builder value( String value ) {
			this.value = value;
			return this;
		}
		
		public Thing build() {
			return new Thing( this );
		}
		
		public Builder owner( String owner ) {
			this.owner = owner;
			return this;
		}
	}
}
