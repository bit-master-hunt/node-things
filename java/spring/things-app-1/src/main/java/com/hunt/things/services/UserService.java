package com.hunt.things.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.things.exceptions.BadRequestException;
import com.hunt.things.models.User;
import com.hunt.things.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public Collection<User> findAll() {
		return userRepository.findAll();
	}

	public User findOne(String id) {
		return userRepository.findById( id ).orElse( null );
	}

	public User deleteUser(String id) {
		User toDelete = findOne( id );
		userRepository.delete( toDelete );
		return toDelete;
	}

	public User createUser( User user ) {
		return userRepository.save(user);
	}

	public User updateUser( User user ) throws BadRequestException {
		User old = findOne( user.getId());
		if( old != null ) {
			return userRepository.save( user );
		} else {
			throw new BadRequestException();
		}
	}
}
