package com.hunt.things.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {
	@Id
	private String id;
	private String email;
	private String first;
	private String last;
	
	public User() {}
	
	private User(Builder b ){
		this.email = b.email;
		this.first = b.first;
		this.last = b.last;
	}
	
	public static class Builder {
		private String id;
		private String email;
		private String first;
		private String last;
		
		public Builder() {}
		
		public Builder email( String email ) {
			this.email = email;
			return this;
		}
		
		public Builder first( String first ) {
			this.first = first;
			return this;
		}
		
		public Builder last( String last ) {
			this.last = last;
			return this;
		}
		
		public User build() {
			return new User( this );
		}
	}
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId( String id ) {
		this.id = id;
	}


	public String getFirst() {
		return first;
	}


	public void setFirst(String first) {
		this.first = first;
	}


	public String getLast() {
		return last;
	}


	public void setLast(String last) {
		this.last = last;
	}
}
