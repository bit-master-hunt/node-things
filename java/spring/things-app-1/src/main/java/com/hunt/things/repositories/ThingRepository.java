package com.hunt.things.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hunt.things.models.Thing;

@Repository
public interface ThingRepository extends MongoRepository<Thing, String> {
	List<Thing> findByOwner(String id);
}
