package com.hunt.files.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.hunt.files.models.Avatar;

@Service
public class FileSystemStorageService {
	private final Path ROOT = Paths.get("/var/app");
	private Map<String,String> idToType = Collections.<String,String>synchronizedMap(new HashMap<>() );

	public Avatar save( MultipartFile file ) throws IOException {
		if( file.isEmpty() ) {
			throw new IOException("Failed to store empty file.");
		}

		String fileType = FilenameUtils.getExtension( file.getOriginalFilename() );
		
		String fileName = UUID.randomUUID().toString();
		InputStream inputStream = file.getInputStream();
		
		FileUtils.copyInputStreamToFile( inputStream, this.ROOT.resolve( fileName ).toFile() );
		idToType.put( fileName, fileType );
		return new Avatar( fileName, fileType );
	}

	public Stream<Path> loadAll() throws IOException {
		return Files.walk(this.ROOT, 1)
				.filter(path -> !path.equals(this.ROOT))
				.map	(this.ROOT::relativize);
	}
	
	public String getContentType( String filename ) {
		return "image/" + idToType.get( filename );
	}

	public Resource loadAsResource( String filename ) throws MalformedURLException, FileNotFoundException {
		Path file = ROOT.resolve( filename );
		Resource resource = new UrlResource( file.toUri() );
		if (resource.exists() || resource.isReadable()) {
			return resource;
		} else {
			throw new FileNotFoundException();
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(ROOT.toFile());
	}

	@PostConstruct
	public void init() throws IOException {
		Files.createDirectories(ROOT);	
	}
}