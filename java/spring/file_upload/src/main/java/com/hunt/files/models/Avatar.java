package com.hunt.files.models;

public class Avatar {
	private String id;
	private String type;
	
	public Avatar( String id, String type ) {
		this.id = id;
		this.type = type;
	}
	
	public Avatar() {
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
