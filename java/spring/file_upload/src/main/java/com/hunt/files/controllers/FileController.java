package com.hunt.files.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.hunt.files.models.Avatar;
import com.hunt.files.services.FileSystemStorageService;

@RestController
public class FileController {
	@Autowired
    private FileSystemStorageService storageService;

    @RequestMapping(value="/files", method=RequestMethod.GET)
    public List<String> listUploadedFiles() throws IOException {						
        return storageService
        		.loadAll()
        		.map( path -> MvcUriComponentsBuilder
        				.fromMethodName(FileController.class,
        								"serveFile", 
        								path.getFileName().toString()).build().toString()
        		)
        		.collect(Collectors.toList());
    }

    @RequestMapping(value="/files/{filename}", method=RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) throws MalformedURLException, FileNotFoundException {
        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
        			.ok()
        			.header( HttpHeaders.CONTENT_TYPE, storageService.getContentType( filename ) )
        			.body(file);
    }

    @RequestMapping(value="/files", method=RequestMethod.POST)
    public Avatar handleFileUpload(@RequestParam("avatar") MultipartFile file) throws IOException {
        return storageService.save(file);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<?> handleStorageFileNotFound(IOException exc) {
        return ResponseEntity.notFound().build();
    }
}
