package com.hunt.survey.models;

import java.util.List;

public class SurveySubmission {
	private List<String> favorites;
	private String status;
	
	private SurveySubmission( List<String> favorites, String status ){
		this.favorites = favorites;
		this.status = status;
	}
	
	public List<String> getFavorites() { 
		return favorites;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setFavorites( List<String> favorites ) {
		this.favorites = favorites;
	}
	
	public void setStatus( String status ) {
		this.status = status;
	}
		
	public static class Builder {
		private List<String> favorites;
		private String status;
		
		public Builder favorites( List<String> favorites ) {
			this.favorites = favorites;
			return this;
		}
		
		public Builder status( String status ) {
			this.status = status;
			return this;
		}
		
		public SurveySubmission build() {
			return new SurveySubmission(this.favorites, this.status);
		}
	}
}
