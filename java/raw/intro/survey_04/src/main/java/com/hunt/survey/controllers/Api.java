package com.hunt.survey.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hunt.survey.models.SurveySubmission;
import com.hunt.survey.services.SurveyService;

@Controller
@RequestMapping(value="/survey")
public class Api {
	@Autowired
	private SurveyService service;
	
	@RequestMapping(method=RequestMethod.GET )
	public String getSurvey() {
		return "survey";
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String submit(@Valid SurveySubmission submission) {
		service.submit(submission);
		return "results";
	}
	
	@ModelAttribute("topics")
	public Map<String,Integer> getTopics() {
		return service.getTopics();
	}
	
	@ModelAttribute("statuses")
	public Map<String,Integer> getStatus() {
		return service.getStatuses();
	}
}
