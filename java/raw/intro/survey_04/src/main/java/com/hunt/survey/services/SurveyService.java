package com.hunt.survey.services;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import com.hunt.survey.models.Status;
import com.hunt.survey.models.SurveySubmission;

@Service
public class SurveyService {
	private List<SurveySubmission> submissions = new LinkedList<>();
	private Map<String, Integer> topics = new TreeMap<>();
	private Map<String, Integer> statuses = new TreeMap<>();

	private List<String> statusKeys = Arrays.asList( new String[] { Status.OTHER.toString(), Status.WI.toString() } );
	private List<String> topicKeys = Arrays.asList(new String[] { "ART", "BIO", "CS", "MTH", "THA", "PSY" });
	
	public SurveyService() {
		for (String key : statusKeys) {
			statuses.put(key, 0);
		}

		for (String key : topicKeys) {
			topics.put(key, 0);
		}
	}
		
	public void submit( SurveySubmission submission ) {
		submissions.add( submission );
		
		for( String topic : submission.getFavorites() ) {
			topics.put( topic, topics.get(topic) + 1 );
		}

		statuses.put( submission.getStatus().toString(), statuses.get( submission.getStatus().toString() ) + 1 );
	}

	public List<SurveySubmission> getSubmissions() {
		return submissions;
	}

	public void setSubmissions(List<SurveySubmission> submissions) {
		this.submissions = submissions;
	}

	public Map<String, Integer> getTopics() {
		return topics;
	}

	public void setTopics(Map<String, Integer> topics) {
		this.topics = topics;
	}

	public Map<String, Integer> getStatuses() {
		return statuses;
	}

	public void setStatuses(Map<String, Integer> statuses) {
		this.statuses = statuses;
	}

	public List<String> getStatusKeys() {
		return statusKeys;
	}

	public void setStatusKeys(List<String> statusKeys) {
		this.statusKeys = statusKeys;
	}

	public List<String> getTopicKeys() {
		return topicKeys;
	}

	public void setTopicKeys(List<String> topicKeys) {
		this.topicKeys = topicKeys;
	}

	public int getSumbissionCount() {
		return this.submissions.size();
	}
}
