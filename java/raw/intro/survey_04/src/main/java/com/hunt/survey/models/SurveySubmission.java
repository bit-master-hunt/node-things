package com.hunt.survey.models;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SurveySubmission {
	@Size(min=3,max=3)
	@NotNull
	private List<String> favorites;
	
	@NotNull
	private Status status;
	
	private SurveySubmission( List<String> favorites, Status status ){
		this.favorites = favorites;
		this.status = status;
	}
	
	public SurveySubmission() {
	}
	
	public List<String> getFavorites() { 
		return favorites;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public void setFavorites( List<String> favorites ) {
		this.favorites = favorites;
	}
	
	public void setStatus( Status status ) {
		this.status = status;
	}
		
	public static class Builder {
		private List<String> favorites;
		private Status status;
		
		public Builder favorites( List<String> favorites ) {
			this.favorites = favorites;
			return this;
		}
		
		public Builder status( Status status ) {
			this.status = status;
			return this;
		}
		
		public SurveySubmission build() {
			return new SurveySubmission(this.favorites, this.status);
		}
	}
}
