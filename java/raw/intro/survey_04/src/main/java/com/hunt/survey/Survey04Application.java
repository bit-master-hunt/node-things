package com.hunt.survey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Survey04Application {

	public static void main(String[] args) {
		SpringApplication.run(Survey04Application.class, args);
	}
}
