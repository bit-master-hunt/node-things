var threeFavorites = function () {
  return $('[name=favorites]:checked').length == 3;
}

var statusChecked = function () {
  return $('[name=status]:checked').length == 1;
}

var isValid = function() {
  return threeFavorites() && statusChecked();
}

var checked = function( ) {
    $('[name=favorites]').prop('disabled', false );
    if( $('[name=favorites]:checked').length == 3 ) {
        $('[name=favorites]:not(:checked)').prop('disabled', true);
    }
}

$(document).ready( function() {
   $('[name=favorites]').click( checked );
} );
