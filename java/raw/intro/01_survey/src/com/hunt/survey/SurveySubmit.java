package com.hunt.survey;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/submit")
public class SurveySubmit extends HttpServlet {
	private static final long serialVersionUID = 6029710796926410115L;
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	                                throws ServletException, IOException {
	        response.setContentType("text/html;charset=UTF-8");
	        PrintWriter out = response.getWriter();
	        String status = request.getParameter("status");
	        String[] favorites = request.getParameterValues("favorites");

	        try {
	            out.println("<html>");
	            out.println("<head>");
	            out.println("<title>Servlet submit</title>");  
	            out.println("</head>");
	            out.println("<body>");
	            out.println("<h1>Servlet submit at " + request.getContextPath () + "</h1>");
	            out.println("status was " + status + "<br/>");
	            out.println("<ul>");
	            for(String topic : favorites) {
	                out.println("<li>" +  topic + "</li>");
	            }
	            out.println("</ul>");
	            out.println("</body>");
	            out.println("</html>");
	        } catch(Exception e) {
	            out.println(e);
	        } finally { 
	            out.close();
	        }
	    } 
	}
