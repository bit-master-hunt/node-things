package com.hunt.survey;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hunt.survey.services.SurveyService;

@WebServlet("/chart")
public class SurveyPieChart extends HttpServlet { 
	private static final long serialVersionUID = 6935434599404288982L;
	private Map<String, Color> sliceColors;
	private SurveyService service;
	
	public void init() {
		service = SurveyService.getInstance();
		sliceColors = new HashMap<>();
		List<Color> colors = Arrays.asList( new Color[]{
				Color.red, 
				Color.green, 
				Color.blue, 
				Color.cyan, 
				Color.magenta, 
				Color.orange
		} );
		
		List<String> keys = service.getTopicKeys();
		IntStream.range( 0, colors.size() )
			.forEach( i -> {
				sliceColors.put( keys.get(i), colors.get(i) );
			}
		);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("image/png");
		OutputStream fout = response.getOutputStream();

		try {
			BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
			Graphics g = image.getGraphics();
			// set background to white
			g.setColor( Color.white );
			g.fillRect( 0,  0,  500,  500);
			
			// make the circle be 400x400
			// Iterate over every slice
			int startAngle = 0;
			for( String topic : service.getTopicKeys() ) {
				double percentage = service.getTopics().get( topic ) / ( 3.0 * service.getSumbissionCount() );
				int endAngle = (int) Math.round( startAngle + percentage * 360 );
				g.setColor( sliceColors.get( topic ) );
				g.fillArc(50, 50, 400, 400, startAngle, endAngle - startAngle );
				startAngle = endAngle;
			}

			ImageIO.write(image, "PNG", fout);
		} catch(Exception e) {
		} finally { 
			fout.close();
		}
	}
}
