import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThingFormComponent } from './thing-form/thing-form.component';
import { ThingListComponent } from './thing-list/thing-list.component';


const routes: Routes = [
  { path : '', component : ThingListComponent },
  { path : 'things/:tid', component: ThingFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
