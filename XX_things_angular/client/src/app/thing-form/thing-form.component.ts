import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { ApiService } from '../api.service';
import { Thing } from '../thing';

@Component({
  selector: 'app-thing-form',
  templateUrl: './thing-form.component.html',
  styleUrls: ['./thing-form.component.css']
})
export class ThingFormComponent implements OnInit {
  thing : Thing;
  constructor( private api : ApiService, private route : ActivatedRoute ) { }

  ngOnInit() {
    let id = this.route.snapshot.params.tid;
    this.api.getThing( id ).subscribe( thing => {
      this.thing = thing;
    })
  }

}
