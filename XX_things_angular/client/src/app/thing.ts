export class Thing {
   id : string;
   name : string;
   value : string;

   constructor( obj : any ) {
      this.id = obj.id;
      this.name = obj.name;
      this.value = obj.value;
   }

}
