import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Thing } from './thing';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  URL : string = '/api/v1/things';

  constructor( private http : HttpClient ) { }

  things() : Observable<Thing[]> {
    return this.http.get<Thing[]>(this.URL);
  }

  createThing( thing ) : Observable<Thing> {
    return this.http.post<Thing>( this.URL, thing );
  }

  getThing( id ) : Observable<Thing> {
    return this.http.get<Thing>( this.URL + '/' + id );
  }

  deleteThing( id ) : Observable<Thing> {
    return this.http.delete<Thing>( this.URL + '/' + id );
  }
}
