import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Thing } from '../thing';

@Component({
  selector: 'app-thing-list',
  templateUrl: './thing-list.component.html',
  styleUrls: ['./thing-list.component.css']
})
export class ThingListComponent implements OnInit {
  things: Thing[];
  newThing : Thing;
  filter : String = '';

  constructor( private api : ApiService ) {     
  }

  ngOnInit() {
    this.newThing = new Thing({});
    this.retrieveThings();
  }

  createThing() {
    if( this.newThing.name && this.newThing.value ) {
      this.api.createThing( this.newThing ).subscribe( thing => {
        this.things.push( thing );
        this.newThing = new Thing({});
      });
   }
  }

  retrieveThings() {
    this.api.things().subscribe( things => {
      this.things = things;
    });
  }
}
