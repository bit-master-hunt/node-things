import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ThingListComponent } from './thing-list/thing-list.component';
import { ThingComponent } from './thing/thing.component';
import { ThingFormComponent } from './thing-form/thing-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ThingListComponent,
    ThingComponent,
    ThingFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
