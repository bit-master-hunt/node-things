var express = require('express');
var router = express.Router();

function Thing( name, value ) {
   function guid() {
      function s4() {
         return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
   }
   
   this.id = guid();
   this.name = name;
   this.value = value;
};

// This is a collection of things
// indexed by thing.id
var thingsDb = {};

var createThing = function( name, value ) {
   var result = new Thing( name, value );
   thingsDb[ result.id ] = result;
   return result;
};

var deleteThing = function( tid ) {
   var result = thingsDb[ tid ];
   delete thingsDb[ tid ];
   return result;
};

var updateThing = function( tid, newThing ) {
   var oldThing = thingsDb[ tid ];
   if( oldThing ) {
      oldThing.name = newThing.name || oldThing.name;
      oldThing.value = newThing.value || oldThing.value;
   }

   return oldThing;
};

function contains( text, key ) {
   return text.toUpperCase().indexOf( key.toUpperCase() ) >= 0;
};

/**************** ROUTES ***********************************/
router.get('/', function(req, res, next) {
   res.sendFile( 'index.html', { root : __dirname + "/../public" } );
});

router.get('/api/v1/things', function( req, res, next ) {
   var filter = req.query.filter || '';
   var result = [];
   for( var key in thingsDb ) {
      result.push( thingsDb[ key ] );
   }

   if( filter ) {
      result = result.filter( thing => (
         thing.id==filter || contains( thing.name, filter ) || contains( thing.value, filter )
      ) );
   }
   res.send( result );
} );

router.get('/api/v1/things/:id', function( req, res, next ) {
   var result = thingsDb[ req.params.id ] || { status : 'no such thing' };
   for( qp in req.query ) {
      result[ qp ] = req[ qp ];
   }
   res.send( result );
} );

router.post('/api/v1/things', function( req, res, next ) {
   var result = createThing( req.body.name, req.body.value );
   res.send( result );
} );

router.delete('/api/v1/things/:tid', function( req, res, next ) {
   var result = deleteThing( req.params.tid );
   res.send( result );
} );

router.put('/api/v1/things/:tid', function( req, res, next ) {
   var result = updateThing( req.params.tid, req.body );
   res.send( result );
} );

module.exports = router;
