var express = require('express');
var router = express.Router();
var uuid = require('uuid');
var users = require('./users');

function Thing( ownerId, name, value ) {
  this.id = uuid();
  this.ownerId = ownerId;
  this.name = name;
  this.value = value;
};

// This is a collection of things
//   indexed by thing id
//   each value is a thing
var thingsDb = {};

var getDelay = function() {
    return Math.random() * 5000;
}

var getThings = function( cb ) {
    setTimeout( () =>  {
            var result = [];
            for( var key in thingsDb ) {
                result.push( thingsDb[ key ] );
            }

            var error = result ? null : { error : 'internal error' };
            cb( error, result );
        }, getDelay() );
}

var getThing = function( ownerId, tid, cb ) {
    setTimeout( () => {
            var result = thingsDb[ tid ];
            var error = result ? null : { error : 'internal error' };
            
            cb( error, result );
        }, getDelay() );
}

var createThing = function( ownerId, name, value, cb ) {
    setTimeout( () => {
            var result = new Thing( ownerId, name, value );
            thingsDb[ result.id ] = result;
            cb( null, result );
        }, getDelay() );
};

var deleteThing = function( ownerId, tid, cb ) {
    setTimeout( () => {
            var result = thingsDb[ tid ];
            if( result && result.ownerId == ownerId ) {
                delete thingsDb[ tid ];
                cb( null, result );    
            } else {
                cb( { error : 'no such thing' } );
            }
        }, getDelay() );
};

var updateThing = function( ownerId, tid, newThing, cb ) {
    setTimeout( () => {
            var oldThing = thingsDb[ tid ];
            if( oldThing ) {
                oldThing.name = newThing.name || oldThing.name;
                oldThing.value = newThing.value || oldThing.value;
            }
            
            cb( null, oldThing );
        }, getDelay() );
};

function contains( text, key ) {
  return text.toUpperCase().indexOf( key.toUpperCase() ) >= 0;
};

/**************** ROUTES ***********************************/

router.all( '/users/:uid/*', function( req, res, next ) {
  var pathUser = users.findById( req.params.uid );
  var authenticatedUser = req.session.user;
  if( authenticatedUser && pathUser && authenticatedUser.id == pathUser.id ) {
    next();
  } else {
    res.redirect( '/' );
  }
} );

router.get('/users/:uid/things', function( req, res, next ) {
  var filter = req.query.filter || '';
  getThings( (error, result) => {
    if( error ){
      res.json( error );
    } else {
      result = result.filter( thing => thing.ownerId == req.params.uid );
      
      if( filter ) {
        result = result.filter( thing => (
          thing.id==filter || contains( thing.name, filter ) || contains( thing.value, filter )
        ) );
      }
      res.json( result );
    }
  });
} );

router.get('/users/:uid/things/:tid', function( req, res, next ) {
  getThing( req.params.tid, (error, result) => {
    if( error) {
      res.status(400).json( error );
    } else {
      var result = result || { status : 'no such thing' };    
      if( result.ownerId == req.params.uid ) {
        res.json( result );
      } else {
        res.status( 404 ).send( result );
      }
    }
  });
});

router.post('/users/:uid/things', function( req, res, next ) {
  createThing( req.params.uid, req.body.name, req.body.value, (error, result) => {
    if( error ){
      res.status( 400 ).json( error );
    } else {
      res.json( result );
    }
  });
});

router.delete('/users/:uid/things/:tid', function( req, res, next ) {
  deleteThing( req.params.uid, req.params.tid, (error, result) => {
    if( error) {
      res.status( 400 ).json( error );
    } else {
      res.send( result );
    }
  });
});

router.put('/users/:uid/things/:tid', function( req, res, next ) {
  updateThing( req.params.uid, req.params.tid, req.body, (error, result) => {
    if( error ) {
      res.status( 400 ).json( error );
    } else {
      res.send( result );
    }
  });
});

module.exports = router;
