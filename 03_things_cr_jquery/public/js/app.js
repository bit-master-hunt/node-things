// Global state variable(s)
var modalState = {
   thing : null
};

/********************************** DOM FUNCTIONS ****************************************/

function makeRow( type, values ) {
   return $(`<tr><${type}>` + values.join(`</${type}><${type}>`) + `</${type}></${type}>` );
}

function updateRow( row, thing ) {
   var props = ['id', 'name', 'value' ];
   var newRow = makeRow( 'td', props.map( p => thing[p] ) );
   newRow.click( (event) => showModal( thing ) );   
   row.replaceWith( newRow );
   thing.row = newRow;
}

function updateTable( things ) {
   var table = $('#table').empty();
   var props = ['id', 'name', 'value' ];

   // make header
   makeRow( 'th', props ).appendTo( table );

   things.forEach( thing => {
      var tr = makeRow( 'td', props.map( p => thing[p] ) );
      tr.click( (event) => showModal( thing ) );
      tr.appendTo( table );
      thing.row = tr;
   } );
};

/************************************** AJAX  *****************************/
function createThing( ) {
   var name = $('#name').val();
   var value = $('#value').val();

   $('#name').val('');
   $('#value').val('');
   
   $.ajax( {
      url : 'api/things',
      method : 'POST',
      data : { name : name, value : value },
      success : retrieveThings
   } );
};

function retrieveThings( ) {
   $.ajax( {
      url : 'api/things',
      method : 'GET',
      success : updateTable
   } );
};

function searchThings( event ) {
   event.preventDefault();
   
   var filter = $('#search').val();
   $('#search').val('');

   if( !filter ) return;
   
   $.ajax( {
      url : 'api/things/',
      method : 'GET',
      data : { filter : filter },
      success : updateTable
   } );
};
