// Global state variable
var state = {
   modal : { thing : null },
   page : { pages : ["login", "content", "editModal" ], page : null },
   user : null
}
   
/********************************** DOM FUNCTIONS ****************************************/
$(document).ready( function() {
   $.ajax( {
      url : '/user',
      method : 'GET',
      success : setUser,
      error : () => setPage( 'login' )
   } );
} );

function setPage( page ) {
   state.page.page = page;
   if( page == 'login' ) {
      $('body').addClass( 'background' );
   } else {
      $('body').removeClass('background');
   }

   if( page == 'content' ) {
      retrieveThings();
   }
   
   state.page.pages.forEach(
      p => {
         var selector = "#" + p;
         state.page.page == p ? $(selector).show() : $(selector).hide();
      }
   );
}

function setUser( user ) {
   state.user = user;
   $('#email').text( user && user.email );
   setPage( user ? 'content' : 'login' );
}

function login(evt) {
   evt.preventDefault();
   var password = $('#login_password').val();
   var username = $('#login_username').val();

   $('#login_password').val('');
   $('#login_username').val('');
   
   $.ajax( {
      url : '/login',
      data :  { "username" : username, "password" : password },
      method : 'POST',
      success : setUser
   } );
}

function logout(evt) {
   $.ajax( {
      url : '/logout',
      method : 'POST',
      success : () => setUser(null)
   } );
}

function makeRow( type, values ) {
   return $(`<tr><${type}>` + values.join(`</${type}><${type}>`) + `</${type}></${type}>` );
}

function updateRow( row, thing ) {
   var props = ['id', 'name', 'value' ];
   var newRow = makeRow( 'td', props.map( p => thing[p] ) );
   newRow.click( (event) => showModal( thing ) );   
   row.replaceWith( newRow );
   thing.row = newRow;
}

function updateTable( things ) {
   var table = $('#table').empty();
   var props = ['id', 'name', 'value' ];

   // make header
   makeRow( 'th', props ).appendTo( table );

   things.forEach( thing => {
      var tr = makeRow( 'td', props.map( p => thing[p] ) );
      tr.click( (event) => showModal( thing ) );
      tr.appendTo( table );
      thing.row = tr;
   } );
};

function showModal( thing ) {
   state.modal.thing = thing;
   if( thing ) {
      $('#nameModal').val( thing.name );
      $('#valueModal').val( thing.value );
      $('#editModal').slideDown();
      $('#content').slideUp();
   } else {
      $('#editModal').slideUp();
      $('#content').slideDown();
   }
}

function cancel() {
   showModal( null );
}


/************************************** AJAX  *****************************/
function createThing( ) {
   var name = $('#name').val();
   var value = $('#value').val();

   $('#name').val('');
   $('#value').val('');
   
   $.ajax( {
      url : 'api/things',
      method : 'POST',
      data : { name : name, value : value },
      success : retrieveThings
   } );
};

function retrieveThings( ) {
   $.ajax( {
      url : 'api/things',
      method : 'GET',
      success : updateTable
   } );
};

function updateThing( ) {
   var newName = $('#nameModal').val();
   var newValue= $('#valueModal').val();
   
   $.ajax( {
      url : 'api/things/' + state.modal.thing.id,
      method : 'PUT',
      success : ( updatedThing ) => { updateRow( state.modal.thing.row, updatedThing ); showModal( null ); },
      data : { id : state.modal.thing.id, name : newName, value : newValue }
   } );
}

function deleteThing( ) {
   var thing = state.modal.thing;
   $.ajax( {
      url : 'api/things/' + thing.id,
      method : 'DELETE',
      success : (thing) => { retrieveThings(); showModal( null ); }
   } );
}

function searchThings( event ) {
   event.preventDefault();
   
   var filter = $('#search').val();
   $('#search').val('');

   if( !filter ) return;
   
   $.ajax( {
      url : 'api/things/',
      method : 'GET',
      data : { filter : filter },
      success : updateTable
   } );
};
