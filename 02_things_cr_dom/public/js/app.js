// Global state variable(s)
var modalState = {
   thing : null
};

/********************************** DOM FUNCTIONS ****************************************/

function empty( node ) {
   while( node.firstChild ) {
      node.removeChild( node.firstChild );
   }
}

function makeRow( type, values ) {
   var row = document.createElement( 'tr' );

   values.forEach( value => {
      var cell = document.createElement( type );
      cell.innerHTML = value;

      row.appendChild( cell );
   } );
   
   return row;
}

function updateRow( row, thing ) {
   var props = ['id', 'name', 'value' ];
   var newRow = makeRow( 'td', props.map( p => thing[p] ) );
   newRow.addEventListner( 'click', (event) => showModal( thing ) );

   row.parent.replaceChild( newRow, row );
   thing.row = newRow;
}

function updateTable( things ) {
   var table = document.getElementById('table');
   empty( table );
   
   var props = ['id', 'name', 'value' ];

   // make header
   table.append( makeRow( 'th', props ) );

   // make data rows
   things.forEach( thing => {
      var tr = makeRow( 'td', props.map( p => thing[p] ) );
      tr.addEventListener( 'click', (event)=>showModal(thing) );
      table.appendChild( tr );
      thing.row = tr;
   } );
};

/************************************** AJAX  *****************************/
function createThing( ) {
   var nameNode = document.getElementById( 'name' );
   var valueNode = document.getElementById( 'value' );

   var name = nameNode.value;
   var value = valueNode.value;

   nameNode.value = '';
   valueNode.value = '';
   
   $.ajax( {
      url : 'api/things',
      method : 'POST',
      data : { name : name, value : value },
      success : retrieveThings
   } );
};

function retrieveThings( ) {
   $.ajax( {
      url : 'api/things',
      method : 'GET',
      success : updateTable
   } );
};

function searchThings( event ) {
   event.preventDefault();

   var filterNode = document.getElementById('search');
   var filter = filterNode.value;
   filterNode.value = '';

   if( !filter ) return;
   
   $.ajax( {
      url : 'api/things/',
      method : 'GET',
      data : { filter : filter },
      success : updateTable
   } );
};
