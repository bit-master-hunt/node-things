var express = require('express');
var router = express.Router();

// { name : { name: ..., value: ..., attrs... } }
let cookies = {};

function createResponse( req, res ) {
  return {
    set : Object.keys( cookies ).map( name => cookies[name] ),
    received : Object.keys( req.cookies ).map( name => ({ name : name, value : req.cookies[name] })  )
  }
}

router.get( '/api/v1/cookies/clear', (req, res, next) => { 
  Object.keys( req.cookies ).forEach( cname => {
    res.cookie( cname, req.cookies[cname], { maxAge : 0 } );
  });

  Object.keys( cookies ).forEach( cname => {
    res.cookie( cname, "destroy", { maxAge : 0 } );
  });

  req.cookies = {};  
  cookies = {};
  res.json( createResponse( req, res ) );
});

router.get( '/cookies', (req, res, next) => {
  res.redirect('/');
});


router.get( '/api/v1/cookies', (req, res, next) => {
  res.send( createResponse( req, res ) );
});

router.post( '/api/v1/cookies', (req, res, next) => {

  let newCookie = { name : req.body.name, value : req.body.value };

  newCookie.attrs = ['domain', 'encode', 'expires', 'httpOnly', 'maxAge', 'path', 'secure', 'sameSite'].reduce( (attrs,val) => {
    if( req.body[val] !== "" ) {
      attrs[val] = req.body[ val ];
    }
    return attrs;
  }, {});

  if( newCookie.attrs.expires ) {
    newCookie.attrs.expires = new Date( newCookie.attrs.expires );
  }

  if( newCookie.attrs.expires && newCookie.attrs.maxAge == 0) {
    delete newCookie.attrs.maxAge;
  }

  console.log( "post: " + JSON.stringify( newCookie ) );

  if( newCookie.name ) {
    cookies[ newCookie.name ] = newCookie;
    res.cookie( newCookie.name, newCookie.value, newCookie.attrs );
    res.send( createResponse( req, res ) );
  } else {
    res.send( creatResponse( req, res ) );
  }

});

router.delete( '/api/v1/cookies/:name', (req,res,next) => {
  delete cookies[req.params.name];
  
  res.send( createResponse( req, res ) );
});

router.all( '/api/v1/*', (req,res,next) => {
  res.send( createResponse( req, res ) );
});

module.exports = router;
