import { Component, OnInit } from '@angular/core';
import { CookieServiceService } from '../cookie-service.service';
import { Cookie } from '../cookie.model';
import { State } from '../state.model';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.css']
})
export class CookiesComponent implements OnInit {
  props : string[] = ['name', 'value', 'domain', 'path', 'expires', 'httpOnly', 'maxAge', 'sameSite' ];
  state : State;
  cookie? : Cookie = { 
    name : '', 
    value : '', 
    domain : '',
    expires : '',
    httpOnly: false,
    maxAge : 0,
    path : '',
    sameSite : false,
    secure: false
  };
  pathToCheck : string;

  constructor(private cookieService : CookieServiceService ) { }

  ngOnInit(): void {    
    this.getState();
  }

  getState() {
    this.cookieService.getCookies().subscribe( state => this.state = state );
  }

  createCookie() {
    let temp = Object.assign( {}, this.cookie );
    temp.path = "/api/v1/" + temp.path;
    this.cookieService.createCookie( temp ).subscribe( state => this.state = state );
  }

  clear() {
    this.cookieService.clear().subscribe( state => this.state = state );
  }

  checkPath() {
    this.cookieService.checkPath( this.pathToCheck ).subscribe( state => this.state = state );
  }

}
