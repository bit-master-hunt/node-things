import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CookiesComponent } from './cookies/cookies.component';

const routes: Routes = [
  {path : 'cookies', component : CookiesComponent },
  { path : '', redirectTo : '/cookies', pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
