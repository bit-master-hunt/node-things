import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cookie } from './cookie.model';
import { State } from './state.model';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class CookieServiceService {
  URL : string = "/api/v1/cookies";

  constructor(private http : HttpClient ) { }

  transform( obs : Observable<State> ) : Observable<State> {
    return obs;
    // return obs.pipe( map( st => {
    //   st.received.forEach( c => c.attrs.forEach( (k : string) => c[k] = c.attrs[k] ) );
    //   st.set.forEach( c => c.attrs.forEach( (k : string) => c[k] = c.attrs[k] ) );

    //   console.log( st );
    //   return st;      
    // } ) );
  }

  getCookies() : Observable<State> {
    return this.transform( this.http.get<State>( this.URL ) );
  }

  createCookie( c: Cookie ) : Observable<State> {
    return this.transform( this.http.post<State>( this.URL, c ) );
  }

  deleteCookie( c : Cookie ) : Observable<State> {
    return this.transform( this.http.delete<State>( this.URL + '/' + c.name ) );
  }

  clear() : Observable<State> {
    return this.transform( this.http.get<State>( this.URL + '/clear' ) );
  }

  checkPath( path : string ) : Observable<State> {
    path = path || "";
    let slash = path.startsWith("/") ? "" : "/";
    return this.transform( this.http.get<State>( "/api/v1" + slash + path ) );
  }
}
