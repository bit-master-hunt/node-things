export class Cookie {
   name : string;
   value : string;

   secure: boolean;
   domain : string;
   expires : string;
   httpOnly: boolean;
   maxAge : number;
   path : string;
   sameSite : boolean;

   attrs?: any;
}
