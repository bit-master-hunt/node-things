// Global state variable(s)
var modalState = {
   thing : null
};

/********************************** DOM FUNCTIONS ****************************************/
$(document).ready( function() {
   $('#editModal').hide();   
   showModal( null );
} );

function makeRow( type, values ) {
   return $(`<tr><${type}>` + values.join(`</${type}><${type}>`) + `</${type}></${type}>` );
}

function updateRow( row, thing ) {
   var props = ['id', 'name', 'value' ];
   var newRow = makeRow( 'td', props.map( p => thing[p] ) );
   newRow.click( (event) => showModal( thing ) );   
   row.replaceWith( newRow );
   thing.row = newRow;
}

function updateTable( things ) {
   var table = $('#table').empty();
   var props = ['id', 'name', 'value' ];

   // make header
   makeRow( 'th', props ).appendTo( table );

   things.forEach( thing => {
      var tr = makeRow( 'td', props.map( p => thing[p] ) );
      tr.click( (event) => showModal( thing ) );
      tr.appendTo( table );
      thing.row = tr;
   } );
};

function showModal( thing ) {
   modalState.thing = thing;
   if( thing ) {
      $('#nameModal').val( thing.name );
      $('#valueModal').val( thing.value );
      $('#editModal').slideDown();
      $('#content').slideUp();
   } else {
      $('#editModal').slideUp();
      $('#content').slideDown();
   }
}

function cancel() {
   showModal( null );
}


/************************************** AJAX  *****************************/
function createThing( ) {
   var name = $('#name').val();
   var value = $('#value').val();

   $('#name').val('');
   $('#value').val('');
   
   $.ajax( {
      url : 'api/things',
      method : 'POST',
      data : { name : name, value : value },
      success : retrieveThings
   } );
};

function retrieveThings( ) {
   $.ajax( {
      url : 'api/things',
      method : 'GET',
      success : updateTable
   } );
};

function updateThing( ) {
   var newName = $('#nameModal').val();
   var newValue= $('#valueModal').val();
   
   $.ajax( {
      url : 'api/things/' + modalState.thing.id,
      method : 'PUT',
      success : ( updatedThing ) => { updateRow( modalState.thing.row, updatedThing ); showModal( null ); },
      data : { id : modalState.thing.id, name : newName, value : newValue }
   } );
}

function deleteThing( ) {
   var thing = modalState.thing;
   $.ajax( {
      url : 'api/things/' + thing.id,
      method : 'DELETE',
      success : (thing) => { retrieveThings(); showModal( null ); }
   } );
}

function searchThings( event ) {
   event.preventDefault();
   
   var filter = $('#search').val();
   $('#search').val('');

   if( !filter ) return;
   
   $.ajax( {
      url : 'api/things/',
      method : 'GET',
      data : { filter : filter },
      success : updateTable
   } );
};
