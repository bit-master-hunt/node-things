var express = require('express');
var router = express.Router();
var db = require('./db');

function contains( text, key ) {
   return text.toUpperCase().indexOf( key.toUpperCase() ) >= 0;
};

/**************** ROUTES ***********************************/
router.get('/', function(req, res, next) {
   res.sendFile( 'index.html', { root : __dirname + "/../public" } );
});

router.get('/api/things', function( req, res, next ) {
   var filter = req.query.filter || '';

   db.findAll( function(err, things ) {
      if( filter ) {
         things = things.filter( thing => (
            thing.id==filter || contains( thing.name, filter ) || contains( thing.value, filter )
         ) );
      }
      res.send( things );
   } );

} );

router.get('/api/things/:id', function( req, res, next ) {

   db.find( req.params.id, (err, thing) => {
      res.send( thing );
   });

} );

router.post('/api/things', function( req, res, next ) {
   db.create( req.body, (err, thing ) => {
      res.send( thing );
   });   
} );

router.delete('/api/things/:tid', function( req, res, next ) {
   db.delete( req.params.tid, (err, thing) => {
      res.send( thing );
   });
} );

router.put('/api/things/:tid', function( req, res, next ) {
   db.update( req.body, (err, thing) => {
      res.send( thing );
   } );
} );

module.exports = router;
