var Thing = require('./thing');

let db = function() {
   // This is a collection of things
   // indexed by thing.id
   let thingsDb = {};

   function findAll(cb) {
      cb( null, Object.values( thingsDb ) );
   }

   function create( thing, cb ) {
      var result = new Thing( thing.name, thing.value );
      thingsDb[ result.id ] = result;
      cb( null, result );
   }

   function remove( tid, cb ) {
      var result = thingsDb[ tid ];
      delete thingsDb[ tid ];
      cb( null, result );
   }

   function update( newThing, cb ) {
      var oldThing = thingsDb[ newThing.id ];
      if( oldThing ) {
         oldThing.name = newThing.name || oldThing.name;
         oldThing.value = newThing.value || oldThing.value;
      }

      cb( null, oldThing );
   }

   return {
      findAll : findAll,
      create : create,
      delete : remove,
      update : update
   }
};

module.exports = db();